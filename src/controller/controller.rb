require './src/helpers/util'

class Controller
  def initialize(game)
    @game = game
    @attached = false
  end

  def attach!
    return if @attached
    @attached = true
    Util::Input.attach
  end

  def detach!
    return unless @attached
    @attached = false
    Util::Input.detach
  end

  def poll(max_wait)
    raw_input = Util::Input.get_single_character(max_wait)
    user_input = Controller.translate(raw_input)
    performed = @game.on_input(user_input) || user_input.nil?
    unless performed
      if ['q', "\u0003", :esc].include?(user_input)
        @game.quit!
        performed = true
      end
    end
    @game.log("unexpected input: #{user_input.to_s.inspect}") unless performed
  end

  def self.translate(keycode)
    case keycode
    when "\e[A"
      :up
    when "\e[B"
      :down
    when "\e[D"
      :left
    when "\e[C"
      :right
    when "\r", "\n"
      :enter
    when "\e"
      :esc
    when "\e[3~"
      :del
    else
      keycode
    end
  end
end
