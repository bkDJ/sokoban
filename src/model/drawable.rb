module Drawable
  def initialize
    @dirty = true
  end

  def dirty!
    @dirty = true
  end

  def dirty?
    @dirty
  end
end
