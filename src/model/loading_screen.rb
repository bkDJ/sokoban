require './src/model/screen'

class LoadingScreen < Screen
  def render(out)
    out << File.read('./src/view/loading_screen.txt')
  end
end
