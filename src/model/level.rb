require './src/model/actor/actor'
require './src/model/actor/crate'
require './src/model/actor/goal'
require './src/model/actor/player'
require './src/model/actor/wall'

require './src/model/drawable'

class Level
  include Drawable
  
  def initialize(template, name)
    @name = name
    @template = template
    reset
  end

  def reset
    @player = nil
    @width = 0
    @actors = []
    @grid_passive = []
    @grid_active = []
    y = 0
    @template.each_line do |tline|
      @grid_passive << []
      @grid_active << []
      x = 0
      tline.split('').each do |tchar|
        add_actors(tchar, x, y)
        x += 1
      end
      @width = x if @width < x
      y += 1
    end
    @height = y
    sync_grids
  end

  def add_actors(character, x, y)
    case character
    when '@'
      @player = Player.new(x, y, self)
      @actors << @player
    when '#'
      @actors << Wall.new(x, y, self)
    when 'o'
      @actors << Crate.new(x, y, self)
    when '.'
      @actors << Goal.new(x, y, self)
    else
      nil
    end
  end

  def actor_at(x, y)
    @grid_active[y][x] || @grid_passive[y][x]
  end

  def goal?(x, y)
    @grid_passive[y][x].is_a?(Goal)
  end

  def on_input(key)
    case key
    when :up
      @player.move(0, -1)
    when :down
      @player.move(0, 1)
    when :left
      @player.move(-1, 0)
    when :right
      @player.move(1, 0)
    when 'r'
      reset
    else
      return false
    end
    true
  end

  def sync_grids
    @height.times do |y|
      @grid_passive[y].fill(nil)
      @grid_active[y].fill(nil)
    end
    @actors.each do |actor|
      grid = actor.active? ? @grid_active : @grid_passive
      grid[actor.y][actor.x] = actor
    end
  end

  def update
    sync_grids
    @actors.each do |a|
      a.update
      dirty! if a.dirty?
    end
  end

  def render_coord(out, x, y)
    actor = actor_at(x, y) || Actor::NONE
    actor.render(out)
  end

  def render(out)
    @height.times do |y|
      @width.times do |x|
        render_coord(out, x, y)
      end
      out << "\n"
    end
  end


# when "\e[A"
#   @game.move(0, -1) # up
# when "\e[B"
#   @game.move(0, 1) # down
# when "\e[D"
#   @game.move(-1, 0) # left
# when "\e[C"
#   @game.move(1, 0) # right
# when 'q', "\u0003"
#   @game.end
# else
end
