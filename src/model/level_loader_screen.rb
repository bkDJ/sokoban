require './src/model/loading_screen'
require './src/model/level'
require './src/model/level_screen'

class LevelLoaderScreen < LoadingScreen
  def load_all_levels
    files = Dir["./src/assets/levels/*"]
    levels = []
    files.each do |filename|
      name = File.basename(filename, ".*")
      buffer = ''
      file = File.open(filename, 'r')
      loop do
        line = file.readline
        line_is_empty = line =~ /\A\s*\z/
        line_is_last = file.eof?
        buffer += line unless line_is_empty
        if (line_is_empty || line_is_last) && !buffer.empty?
          levels << Level.new(buffer, "#{name} \##{levels.length+1}")
          buffer = ''
        end
        break if line_is_last
      end
    end
    @next = LevelScreen.new(@game, levels[0])
  end

  def update
    @tick = (@tick || -1) + 1
    if @tick == 1
      load_all_levels
    end
  end
end
