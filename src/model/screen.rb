class Screen
  attr_reader :input_handler
  attr_reader :next

  def initialize(game)
    @game = game
  end

  def on_input(key)
    false
  end

  def update
  end

  def render(out)
  end
end
