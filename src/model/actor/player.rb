require './src/model/actor/actor'

class Player < Actor
  def initialize(x, y, level)
    super
    @active = true
    @blocked_ticks = 0
  end

  def move(dx, dy)
    actor = @level.actor_at(x + dx, y + dy)
    case actor
    when Wall
      blocked!
      actor.in_the_way!
      false
    when Crate
      if actor.move(dx, dy)
        super
      else
        blocked!
        false
      end
    else
      super
    end
  end

  def blocked!
    @state = :blocked
    @blocked_ticks = 30 # 0.5 seconds @ 60fps
  end

  def update
    if @blocked_ticks > 0
      @blocked_ticks -= 1
      dirty! if @blocked_ticks == 0
    else
      @state = @level.goal?(x, y) ? :success : :normal
    end
  end
end
