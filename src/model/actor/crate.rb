require './src/model/actor/actor'

class Crate < Actor
  def initialize(x, y, level)
    super
    @active = true
  end

  def move(dx, dy)
    actor = @level.actor_at(x + dx, y + dy)
    case actor
    when Wall
      false
    when Crate
      false
    else
      super
    end
  end

  def update
    @state = @level.goal?(x, y) ? :success : :normal
  end
end
