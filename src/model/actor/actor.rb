require './src/model/drawable'

class Actor
  include Drawable
  attr_reader :x
  attr_reader :y
  attr_reader :state

  def initialize(x, y, level)
    @x = x
    @y = y
    @level = level
    @active = false
    @state = :normal
  end

  NONE = Actor.new(-1, -1, nil)

  def move(dx, dy)
    @x += dx
    @y += dy
    true
  end

  def passive?
    !@active
  end

  def active?
    @active
  end

  def update
  end

  def render(out)
    name = self.class.name.downcase
    state = self.state.to_s
    out << File.read("./src/view/actor/#{name}_#{state}.txt").chomp
    @dirty = false
  end
end
