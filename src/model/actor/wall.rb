require './src/model/actor/actor'

class Wall < Actor
  def initialize(x, y, level)
    super
    @in_the_way_ticks = 0
  end

  def in_the_way!
    @in_the_way_ticks = 100
    @state = :in_the_way
  end

  def update
    if @in_the_way_ticks > 0
      @in_the_way_ticks -= 1
      dirty! if @in_the_way_ticks == 0
    else
      @state = :normal
    end
  end
end
