require './src/model/screen'

class LevelScreen < Screen
  def initialize(game, level)
    super(game)
    @level = level
  end

  def on_input(key)
    @game.dirty! if @level.on_input(key)
  end

  def update
    @level.update
    @game.dirty! if @level.dirty?
  end

  def render(out)
    @level.render(out)
  end
end
