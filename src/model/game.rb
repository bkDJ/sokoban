require './src/helpers/offscreen_buffer'

require './src/model/level'
require './src/model/level_loader_screen'

require './src/controller/controller'

class Game
  attr_reader :levels

  def initialize
    @running = true
    @dirty = true
    @offscreen_buffer = OffscreenBuffer.new
    @controller = Controller.new(self)
    @controller.attach!
    @screen = LevelLoaderScreen.new(self)
  end

  def run!
    while running?
      update
      render
    end
  rescue => ex
    @controller&.detach!
    raise
  end

  def running?
    @running
  end

  def dirty!
    @dirty = true
  end

  def dirty?
    @dirty
  end

  def quit!
    @controller.detach!
    puts "\nQuitting game..."
    @running = false
  end

  def log(str)
    @log = str
    dirty!
  end

  def on_input(key)
    @screen.on_input(key)
  end

  def update
    @controller.poll(0.016)
    @screen.update
    if @screen.next
      @screen = @screen.next
      dirty!
    end
  end

  def render
    return unless dirty?
    @dirty = false
    @offscreen_buffer.clear
    @screen.render(@offscreen_buffer)
    puts `clear`
    puts @offscreen_buffer
    puts "\n\n"
    puts @log if @log
  end
end
