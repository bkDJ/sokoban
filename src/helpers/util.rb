require 'timeout'
require 'io/wait'

module Util
  class Input
    def self.attach
      @old_state = `stty -g`;
      `stty raw -echo`
    end

    def self.detach
      `stty #{@old_state}`
    end

    def self.get_single_character(max_wait = -1)
      input = nil
      if (max_wait && max_wait >= 0)
        Timeout.timeout(max_wait) do
          input = $stdin.getc.chr
        end
      else
        input = $stdin.getc.chr
      end
      if input == "\e"
        while $stdin.ready?
          input += $stdin.read_nonblock(1000)
        end
        # ignore multiple escape sequences
        second_escape_index = input.index("\e", 1)
        if second_escape_index
          input = input[0...second_escape_index]
        end
      end
      input
    rescue
    end
  end
end
