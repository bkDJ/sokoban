class OffscreenBuffer
  def initialize
    @buffer = ''
  end

  def clear
    @buffer = ''
  end

  def <<(obj)
    @buffer += obj.to_s.gsub(/\r?\n/, "\r\n")
    self
  end

  def to_s
    @buffer
  end
end
